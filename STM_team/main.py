"""
DESCRIPTION OF THE MODULE
This module calculates the minimum subnet for a list of ip addresses.
The module performs calculations for IPv4 and IPv6
"""
import os
import sys

class Analysis:
    """
    Class describing the calculation of the subnet mask.

    Args:
        filename(str): the name of the source file with the list of IP addresses.
        ipv(str): version of IP. Values: '4' or '6'

    Attributes:
        iDataFilename(str): Uses for contain name of the file with ip list
        oResFilename(str): Uses for contain name of the file for printing the result
        oErrFilename(str): Uses for collecting bad ip addresses with pointing the reason
    """
    iDataFilename = ''
    oResFilename = 'result.txt'
    oErrFilename = 'errors.txt'
    __ipv = ''

    def __init__(self, filename: str, ipv: str) -> None:
        """
        Constructor for class Analysis

        :param filename: name of the file which contains the list of ip addresses
        :param ipv: version of ip addresses in this file
        """
        Analysis.iDataFilename = filename
        self.set_IPversion(ipv)
        self.ipList = list()
        self.result = ''

    def show_info(self) -> None:
        """
        This method printing information about analysis status
        """
        print('\nActual data:')
        print('IP ver =', self.get_IPver())
        print('Good ip list:')
        if self.get_IPver() == '4':
            [print('.'.join(i_ip)) for i_ip in self.ipList]
        elif self.get_IPver() == '6':
            [print(':'.join(i_ip)) for i_ip in self.ipList]

        print('Subnet with mask: ', self.result)

    def set_IPversion(self,ipv: str) -> None:
        """
        Setter mathod for writing of the IP version to the protected variable at the class Analysis
        :param ipv: versoin of ip which should be analysed ('4' or '6')
        """
        if ipv == '4' or ipv == '6':
            Analysis.__ipv = ipv
        else:
            print('Incorrect type of IP version. (User input data)')
            exit(-1)

    def get_IPver(self) -> str:
        """
        Getter method for read value from protected variable at the class Analysis
        :return: __ipv
        :rtype: str
        """
        return Analysis.__ipv

    def init(self, filename) -> None:
        """
        Method for creating the path to file and checking
        if file exists and deleting them before new procession

        :param filename: name of the file to be deleted
        """
        FilePath = os.path.abspath(os.path.join(os.getcwd(), filename))
        if Analysis.oErrFilename in os.listdir(os.getcwd()):
            os.remove(FilePath)

    def readFromFile(self) -> None:
        """
        This method uses for reading the data from the file with ip addresses
        It read data and calls "checkPrepare" method for checking this data
        """
        iFilePath = os.path.abspath(os.path.join(os.getcwd(), Analysis.iDataFilename))
        try:
            with open(iFilePath, 'r', encoding='UTF-8') as iFile:
                for i_line in iFile:
                    self.checkPrepare(i_line)

        except FileNotFoundError:
            print('File "{filename}" not found'.format(filename = Analysis.iDataFilename))

    def checkPrepare(self, txtLine: str) -> None:
        """

        This method uses for cutting literals in the end if it exists.
        Called in self.readFromFile(self)
        Calls 2 methods for checking read ip-data for IPv4 or IPv6.

        """
        # cut off '\n'
        if txtLine.endswith('\n'):
            txtLine = txtLine[:-1]

        if self.__ipv == '4':
            self.prepareV4(txtLine)
        elif self.__ipv == '6':
            self.prepareV6(txtLine)
        else:
            exit(-1)

    def prepareV6(self, txtLine: str) -> None:
        """
        This method uses only for checking IPv6 addresses.
        Address will be written to the Error-file, If ip address is bad.
        Address will be written to the self.ipList, If ip address is good.
        Called in checkPrepare(self, txtLine: str)

        :param txtLine: ip address which was read from the file with ip-list
        :raise IndexError: If ip address has incorrect syntax
        :raise ValueError: If ip address has sybols not in HEX-code
        :finally decide to write ip address to the Error-file or to the self.ipList if it's good
        """
        # init variables
        badIp = False
        reason = ''

        ipSplitted = txtLine.split(':')
        try:
            ipSplitted = ([int(i_segment, 16) if i_segment != '' else '' for i_segment in ipSplitted])
            if (len(ipSplitted) < 8 and not('' in ipSplitted)) or\
                (len(ipSplitted) > 8) or \
                    (ipSplitted.count('') > 1):
                raise IndexError

            elif (len(ipSplitted) < 8) and ('' in ipSplitted): # if ip good
                pasteIdx = ipSplitted.index('')
                ipSplitted.remove('')
                for _ in range(8 - len(ipSplitted)):
                    ipSplitted.insert(pasteIdx, 0)

            ipSplitted = [hex(i_elem)[2:] for i_elem in ipSplitted]

        except ValueError:
            badIp = True
            reason = 'Some of its address elements are not in the HEX-code system'

        except IndexError:
            badIp = True
            reason = 'Incorrect format of IPv6 address'

        finally:
            # create path to error file
            errFilePath = os.path.abspath(os.path.join(os.getcwd(), Analysis.oErrFilename))
            if badIp:
                with open(errFilePath, 'a', encoding='UTF-8') as oErrFile:
                    oErrFile.write('{ip} Reason: {reason} \n'.format(ip=txtLine, reason=reason))
            else:
                self.ipList.append(ipSplitted)

    def prepareV4(self, txtLine: str) -> None:
        """
        This method uses only for checking IPv4 addresses.
        Address will be written to the Error-file, If ip address is bad.
        Address will be written to the self.ipList, If ip address is good.
        Called in checkPrepare(self, txtLine: str)

        :param txtLine: ip address which was read from the file with ip-list
        """

        # init variables
        badIp = False
        reason = ''

        ipSplitted = txtLine.split('.')

        if  not (len(ipSplitted) == 4 and txtLine.count('.') == 3):
            badIp = True
            reason += 'IPv4 address must have 4 numbers separated by 3 dots. '

        elif False in [i_elem.isdigit() for i_elem in ipSplitted]:
            badIp = True
            reason += 'Elements of the ip address must be numbers. '

        elif False in [0 <= int(i_elem) <= 255 for i_elem in ipSplitted]:
            badIp = True
            reason += 'Each segment must be between 0 and 255. '

        # create path to error file
        errFilePath = os.path.abspath(os.path.join(os.getcwd(), Analysis.oErrFilename))

        if badIp:
            with open(errFilePath, 'a', encoding='UTF-8') as oErrFile:
                oErrFile.write('{ip} Reason: {reason} \n'.format(ip = txtLine, reason = reason))
                oErrFile.close()
        else:
            self.ipList.append(ipSplitted)

    def calcMask(self) -> None:
        """
        This method uses for calculating mask for the minimal subnet for IPv4 or prefix for IPv6
        It reads data from self.ipList and writes the result to self.result
        """
        if self.get_IPver() == '4':
            num_segments = 4
            bitPerSegment = 8
        elif self.get_IPver() == '6':
            num_segments = 8
            bitPerSegment = 16

        key_segment = ''
        sameSegments = 0

        for i_segment in range(num_segments):
            col = [i_ip[i_segment] for i_ip in self.ipList] # column in matrix of ip addresses
            # print(col)
            if not (all([n == col[0] for n in col])):
                sameSegments = i_segment
                key_segment = max(col) # found a key segment
                break

        if self.get_IPver() == '4':
            keySegmentBin = bin(int(key_segment, 10))[2:]
        elif self.get_IPver() == '6':
            keySegmentBin = bin(int(key_segment, 16))[2:]

        #calculating the mask
        mask = sameSegments * bitPerSegment + bitPerSegment - len(keySegmentBin)
        subnetList = [str(i_elem)
                  if i_num < sameSegments else '0'
                  for i_num, i_elem in enumerate(self.ipList[0])]

        # make a result to the correct format
        if self.get_IPver() == '4':
            subnet = '.'.join(subnetList)
            self.result = subnet + '/' + str(mask)

        elif self.get_IPver() == '6':
            for i in range(len(subnetList) - 1, 0, -1):
                if subnetList[i] == '0':
                    subnetList.pop(i)
                else:
                    break

            subnet = ':'.join(subnetList)
            self.result = subnet + '::/' + str(mask)

    def writeResult(self) -> None:
        """
        This method uses for write the result to the 'result.txt' file
        """
        oFilePath = os.path.abspath(os.path.join(os.getcwd(), Analysis.oResFilename))
        with open(oFilePath, 'w', encoding='UTF-8') as oFile:
            oFile.write(self.result)

    def execute(self) -> None:
        """
        This method uses for execute all operations: read, check, calculate, write a result

        :except IndexError appears if there not enough ip addresses for analysis or all ip addresses was marking as bad.
        The reason you can find in "errors.txt" file
        """
        try:
            self.init(Analysis.oResFilename)
            self.init(Analysis.oErrFilename)
            self.readFromFile()
            if len(self.ipList) < 2:
                raise IndexError

            self.calcMask()
            # self.show_info()
            self.writeResult()

        except IndexError:
            print('Insufficient data to calculate the mask')

def main() -> None:
    """
    This is a main subprogram uses for:
    - reading arguments (if it called by CLI)
    - creating an object for analysis
    - starting analysis
    """

    if len(sys.argv) == 3:
        filename = sys.argv[1]
        ipVersion = sys.argv[2]
    else:
        filename = 'IPv4.txt'
        ipVersion = '4'

    analys = Analysis(filename = filename, ipv = ipVersion)
    analys.execute()

main()

