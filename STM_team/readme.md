    DESCRIPTION OF THE MODULE               

    This module calculates the minimum subnet for a list of ip addresses.                                                 
    The module performs calculations for IPv4 and IPv6  

    Files IPv6.txt and IPv4.txt are contains ip-address lists.
    After executing this script you can open the result.txt to see a calculated mask.

    Errors.txt file appears if you have some mistakes at source files with ip addresses (IPv4, IPv6)
    This file contain bad ip-addresses and the reason.


    HOW TO USE
    for using from CLI:
        python main.py IPv6.txt 6       : for IPv6  
        python main.py IPv4.txt 4       : for IPv4

    TIME EFFICIENCY ASSESSMENT (Big O)

    self.init(Analysis.oResFilename)
        O(1)            create path
        O(1)            delete file

    self.readFromFile()
        O(A * (         for each line in source file. For example A lines
                O(1)    read line from file
                O(1)    slice of str
                O(N)    split string to list
                O(1)    get lenght of the list
                O(N)    count '.' in the list
                O(N)    paste zeros in the list
                O(1)    paste a link to self.ipList
               )   

    self.calcMask()
        O(B * A)        get a column from 2d matrix. (B - amount of the segments in ip address, A - amount of ip addresses)  
        O(N)            get maxumal from list
        O(1)            convert to dec system
        O(1)            convert to bin system
        O(1)            calculating of the mask
        O(N)            make the list of mask
        O(N)            join list to str
        O(1)            print result

    self.writeResult()
        O(1)            create path
        O(1)            open file

    TOTAL TIME BIG O
    2 * (O(1) + O(1)) + 
    + O(A * (O(1) + O(1) + O(N) + O(1) + O(N) + O(N) + O(1))) + 
    + O(B * A) + O(N) + O(1) + O(1) + O(1) + O(1) + O(N) + O(N) + O(1) +
    + O(1) + O(1)



